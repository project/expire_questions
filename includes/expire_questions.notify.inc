<?php

/**
 * @file
 * Notification functions for the 'Expire Questions' module
 * 
 * @author Chip Cleary
 * 
 */

 /* 
  * The file provides notification functions
  *    1. Notify question authors when their questions are able to be extended
  *    2. Notify question authors when their questions have expired
  */

/*
 * Add settings to the notification form
 */ 
function _expire_questions_notify_settings(&$form) {

  $form['expire_questions']['notifications_settings'] = array(
    '#type' => 'fieldset',
    '#description' => t('Settings for notification emails.'),
    '#title' => t('Notification Email Settings'),
  );

  $form['expire_questions']['notifications_settings']['question_can_be_extended'] = array(
    '#type' => 'fieldset',
    '#description' => t('Notify question author that a question can now be extended.'),
    '#title' => t('Extension Notification'),
  );

  $form['expire_questions']['notifications_settings']['question_can_be_extended']['expire_questions_notify_extension_available_p'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Notify authors when they can extend expiration of their questions?'),
    '#default_value' => variable_get('expire_questions_notify_extension_available_p', FALSE),
  );	

  $form['expire_questions']['notifications_settings']['question_can_be_extended']['expire_questions_question_can_be_extended_notification_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject line'),
    '#default_value' => variable_get('expire_questions_question_can_be_extended_notification_subject', ''),
    '#description' => t('Dynamic variables available: !question_user_name, !question_title, !question_url, !question_expiration, and !site'),
    '#required' => TRUE
  );

    $form['expire_questions']['notifications_settings']['question_can_be_extended']['expire_questions_question_can_be_extended_notification_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#default_value' => variable_get('expire_questions_question_can_be_extended_notification_body', ''),
    '#description' => t('Dynamic variables available: !question_user_name, !question_title, !question_url, !question_expiration, and !site'),
    '#required' => TRUE
  );

  $form['expire_questions']['notifications_settings']['question_expired'] = array(
    '#type' => 'fieldset',
    '#description' => t('Notify question author that a question has expired.'),
    '#title' => t('Expiration Notification'),
  );

  $form['expire_questions']['notifications_settings']['question_expired']['expire_questions_notify_question_expired_p'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Notify author after a question expires?'),
    '#default_value' => variable_get('expire_questions_notify_question_expired_p', FALSE),
  );

  $form['expire_questions']['notifications_settings']['question_expired']['expire_questions_question_expired_notification_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject line'),
    '#default_value' => variable_get('expire_questions_question_expired_notification_subject', ''),
    '#description' => t('Dynamic variables available: !question_user_name, !question_title, !question_url, !question_expiration, and !site'),
    '#required' => TRUE
  );

    $form['expire_questions']['notifications_settings']['question_expired']['expire_questions_question_expired_notification_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#default_value' => variable_get('expire_questions_question_expired_notification_body', ''),
    '#description' => t('Dynamic variables available: !question_user_name, !question_title, !question_url, !question_expiration, and !site'),
    '#required' => TRUE
  );
  
}


function _expire_questions_notify_question_can_be_extended($question) {
  _expire_questions_notify(
    $question,
    variable_get('expire_questions_question_can_be_extended_notification_subject', ''),
    variable_get('expire_questions_question_can_be_extended_notification_body', '')
  );
}

function _expire_questions_notify_question_expired($question) {
  _expire_questions_notify(
    $question,
    variable_get('expire_questions_question_expired_notification_subject', ''),
    variable_get('expire_questions_question_expired_notification_body', '')
  );
}

function _expire_questions_notify($question, $subject_template, $body_template) {
 
  global $user;
  
  $question_user = user_load($question->uid);

  $params = array(
    '!question_user_name'  => $question_user->name, 
    '!question_title'      => $question->title, 
    '!question_url'        => url('node/'. $nid, array('absolute' => TRUE, 'target' => '_blank')),
    '!question_expiration' => date(DATE_RFC822, $question->field_question_expiration[0]['value']),
    '!site'                => variable_get('site_name', 'drupal'),
    );

  $language = user_preferred_language($question_user);

  $headers['MIME-Version'] = '1.0';
  $headers['Content-Type'] = "text/html";
  $headers['From'] = variable_get('site_mail', ini_get('sendmail_from'));
 
  $message = array(
    'headers' => $headers,
    'to'      => $question_user->mail,
    'from'    => variable_get('site_mail', ini_get('sendmail_from')),
    'subject' => t($subject_template, $params,  $language->language),
    'body'    => t($body_template, $params,  $language->language),
    );

  drupal_mail_send($message);

}
