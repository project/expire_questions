<?php

/**
 * @file
 * This include file provides form functions for the expire_questions module
 * 
 * @author Chip Cleary
 * 
 */

/*
 * Provide extensions to the systems settings form for the Answers module
 * Note: this is called via hook_form_alter (not by using hook_menu for this module
 * 
 */
 
/*
 * Create an array of settings to add to the Answers system settings form
 */ 
function _expire_questions_settings(&$form) {

  $form['expire_questions'] = array(
    '#type'          => 'fieldset',
    '#description'   => t('Settings for whether questions are expired after time has passed. Expired questions are locked.'),
    '#title'         => t('Question Expiration Settings'),
    '#weight'        => 1,
  );

  $form['expire_questions']['basic_settings'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Basic Settings'),
  );

  $form['expire_questions']['basic_settings']['expire_questions_p'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Enable questions to expire?'),
    '#default_value' => variable_get('expire_questions_p', FALSE),
  );

  $form['expire_questions']['basic_settings']['expire_questions_days_open'] = array(
    '#type'          => 'textfield',
    '#title'         => t('How long until questions expire?'),
    '#description'   => t('Days questions remain open'),
    '#default_value' => variable_get('expire_questions_days_open', 7),
    '#size'          => 3,
    '#element_validate' => array('expire_questions_validate_positive_integer_p'),
  );

  $form['expire_questions']['extension_settings'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Extension Settings'),
  );

  $form['expire_questions']['extension_settings']['expire_questions_max_extension_count'] = array(
    '#type'          => 'select',
    '#title'         => t('How many times are authors allowed to extend expiration?'),
    '#options'       => array(
      '0' => t('None'),
      '1' => '1',
      '2' => '2',
      '3' => '3',
      '4' => t('Unlimited'),
    ),
    '#default_value' => variable_get('expire_questions_max_extension_count', 0),
  );

  $form['expire_questions']['extension_settings']['expire_questions_extension_duration_days'] = array(
    '#type'          => 'textfield',
    '#title'         => t('How long are extensions?'),
    '#description'   => t('Number of days expiration is extended?'),
    '#default_value' => variable_get('expire_questions_extension_duration_days', 7),
    '#size'          => 3,
    '#element_validate' => array('expire_questions_validate_positive_integer_p'),
  );

  $form['expire_questions']['extension_settings']['expire_questions_extension_available_days'] = array(
    '#type'          => 'textfield',
    '#title'         => t('How long before expiration do authors become allowed to extend expiration?'),
    '#description'   => t('Number of days before expiration that it becomes possible to extend expiration?'),
    '#default_value' => variable_get('expire_questions_extension_available_days', 7),
    '#size'          => 3,
    '#element_validate' => array('expire_questions_validate_positive_integer_p'),
  );

  // add in a submit handler *before* the standard handler
  $form['#submit'][] = 'expire_questions_update_settings_submit';

  // move the submit buttons to the bottom
  $form['buttons']['#weight'] = 100;
  
}

function expire_questions_validate_positive_integer_p($element, &$form_state) {
  $i = $element['#value'];
  if (!is_numeric($i) || $i < 0 || $i != round($i)) {
    form_error($element, t('The field "') . $element['#title'] . t('" must be a positive integer.'));
  }
}

function expire_questions_update_settings_submit ($form, &$form_state) {
  // reset the expiration data for all questions (given that durations and # of extensions may have changed)
  _expire_questions_reset_expirations();
  // reset the question locks (given that some questions that had expired should perhsps be alive and vice versa
  answers_reset_question_locks();
  watchdog('expire_questions', 'Settings updated, Expirations reset');
}


