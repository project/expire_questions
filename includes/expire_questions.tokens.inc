<?php

/**
 * @file
 * This file adds tokens used by Expire_Questions
 * 
 * @author Chip Cleary
 * 
 */

/*
 * Implement hook_token_list 
 */
function expire_questions_token_list($type = 'all') {
  $tokens = array();
  if ($type == 'node' || $type == 'all') {
    $tokens['node']['created-since-addendum'] = t("Node created date - interval - short");
    $tokens['node']['question-interval-until-expiration-addendum'] = t("Interval until the question expires");
  }
  return $tokens;
}

/*
 * Implement hook_token_values 
 */
function expire_questions_token_values($type, $object = NULL, $options = array()) {
  $tokens = array();
  if ($type == 'node' && $object->type == 'question') {
 
   // ['question-interval-until-expiration-addendum']
    $interval = $object->field_question_expiration[0]['value'] - time();
    $output = '';
    if (!$object->field_question_locked_p[0]['value'] && ($interval > 0)) { 
      $output = ' - ' . format_interval($interval, 1) . t(' left to answer');
    }
    $tokens['question-interval-until-expiration-addendum'] = $output;
	
    // ['created-since-addendum']
    $interval = time() - $object->created;
    $tokens['created-since-addendum'] = ' - ' . format_interval($interval, 1) . t(' ago');
  }
  return $tokens;
}

