<?php

/**
 * Implementation of hook_strongarm().
 */
function expire_questions_strongarm() {
  $export = array();
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'expire_questions_cron_last_run';
  $strongarm->value = 1321825336;

  $export['expire_questions_cron_last_run'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'expire_questions_days_open';
  $strongarm->value = '7';

  $export['expire_questions_days_open'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'expire_questions_extension_available_days';
  $strongarm->value = '3';

  $export['expire_questions_extension_available_days'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'expire_questions_extension_duration_days';
  $strongarm->value = '5';

  $export['expire_questions_extension_duration_days'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'expire_questions_lock_question_p';
  $strongarm->value = 0;

  $export['expire_questions_lock_question_p'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'expire_questions_max_extension_count';
  $strongarm->value = '3';

  $export['expire_questions_max_extension_count'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'expire_questions_notify_extension_available_p';
  $strongarm->value = 1;

  $export['expire_questions_notify_extension_available_p'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'expire_questions_notify_question_expired_p';
  $strongarm->value = 1;

  $export['expire_questions_notify_question_expired_p'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'expire_questions_p';
  $strongarm->value = 0;

  $export['expire_questions_p'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'expire_questions_question_can_be_extended_notification_body';
  $strongarm->value = '<p style="font-family: Georgia, sans-serif; font-size: 12px;">
Dear !question_user_name,
<br><br>
You had asked "!question_title". 

Your question is scheduled to expire in !question_expiration days. If you like, you may extend how long it remains open.
<br><br>
To view your question, <a href="!question_url" target="_blank">click here</a>.
<br>
</p>
<hr>
<p style="font-family: Georgia, sans-serif; font-size: 12px; font-style: italic;  color: #00CC00;">
This is an automatic message from the team at !site.</i>
</p>';

  $export['expire_questions_question_can_be_extended_notification_body'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'expire_questions_question_can_be_extended_notification_subject';
  $strongarm->value = 'You may extend how long your question remains open: "!question_title"!';

  $export['expire_questions_question_can_be_extended_notification_subject'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'expire_questions_question_expired_notification_body';
  $strongarm->value = '<p style="font-family: Georgia, sans-serif; font-size: 12px;">
Dear !question_user_name,
<br><br>
Your question "!question_title" has expired and is no longer open to new answers.
<br><br>
To view your question, <a href="!question_url" target="_blank">click here</a>.
<br>
</p>
<hr>
<p style="font-family: Georgia, sans-serif; font-size: 12px; font-style: italic;  color: #00CC00;">
This is an automatic message from the team at !site.</i>
</p>';

  $export['expire_questions_question_expired_notification_body'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'expire_questions_question_expired_notification_subject';
  $strongarm->value = 'Your question "!question_title" has been closed';

  $export['expire_questions_question_expired_notification_subject'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'submitted_by_comment_question';
  $strongarm->value = '';

  $export['submitted_by_comment_question'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'submitted_by_question';
  $strongarm->value = 'Contributed by [author-link] [created-since-addendum] [question-interval-until-expiration-addendum]';

  $export['submitted_by_question'] = $strongarm;
  return $export;
}
